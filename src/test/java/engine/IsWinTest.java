package engine;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.VisibleForTesting;
import org.junit.Assert;
import org.junit.Test;


public class IsWinTest {

    @Test
    public void shouldReturnFalseForAll0Numbers() {
        int[] buttons={1,1,0,0,0,0,0,0,0};
        IsWin isWinChecker = new IsWin();
        boolean isWin = isWinChecker.isWin(buttons);
        Assert.assertFalse(isWin);
    }

    @Test
    public void shouldReturnTrueForA_111AtTheBegining() {
        int[] buttons={1,1,1,0,0,0,0,0,0};
        IsWin isWinChecker = new IsWin();
        boolean isWin = isWinChecker.isWin(buttons);
        Assert.assertTrue(isWin);
    }

}
