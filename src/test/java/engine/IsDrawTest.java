package engine;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class IsDrawTest {

    @Test
    public void shouldReturnTrueFor9() {
        //given
        int moveCounter = 9;
        //when (this code we tests)
        boolean isDraw = IsDraw.isDraw(moveCounter);
        //then
        assertThat(isDraw).isTrue();
    }

    @Test
    public void shouldReturnFalseFor8() {
        //given
        int moveCounter = 8;
        //when (this code we tests)
        boolean isDraw = IsDraw.isDraw(moveCounter);
        //then
        assertThat(isDraw).isFalse();
    }

    @Test
    public void shouldReturnFalseFor0() {
        //given
        int moveCounter = 0;
        //when (this code we tests)
        boolean isDraw = IsDraw.isDraw(moveCounter);
        //then
        assertThat(isDraw).isFalse();
    }
}
