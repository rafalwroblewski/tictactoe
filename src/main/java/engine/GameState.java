package engine;
public class GameState implements EngineInterface {

    private boolean playerX;
    private int[] button;
    private FieldStatus fieldStatus;
    private int moveCounter = 0;

    public GameState(){
        this.playerX = false;
        this.button = new int[9];
    }

    private boolean isAvaileble(int buttonId){

        return(button[buttonId]==0);
    }

    FieldStatus clicked (int buttonId){
        FieldStatus returned;
        if (isAvaileble(buttonId)) {
            moveCounter++;
            switchPlayer(playerX);
            if (playerX) {
                button[buttonId] = 1;
                return fieldStatus.X;
            } else {
                button[buttonId] = -1;
                return FieldStatus.O;
            }
        }
        return fieldStatus.Z;
    }

    private void switchPlayer(boolean currentPlayer) {
        playerX = !currentPlayer;
    }

    @Override
    public void newGame() {
        NewGame.newGame(button);
    }

    @Override
    public AfterMoveStatus onClick(int buttonId) {
        AfterMoveStatus afterMoveStatus = new AfterMoveStatus();
        afterMoveStatus.setFieldStatus(clicked(buttonId));
        afterMoveStatus.setCommunicate(communicate());
        return afterMoveStatus;
    }

    private String communicate() {
        if (IsWin.isWin(button)){
            if (playerX) return "Player X is winner!";
            return "Player O is winner!";
        }
        if (IsDraw.isDraw(moveCounter)) return "DRAW!";
        return "";
    }
}
