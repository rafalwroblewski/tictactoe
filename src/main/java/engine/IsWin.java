package engine;

public class IsWin {
    //zwraca true jeśli w tabeli jest wygrany
    //zwraca false jeśli nie ma wygranego (remis lub gra w toku)
    public static boolean isWin(int[] buttons) {

        if (isEqual(buttons, 0, 1, 2)) return true;
        else if (isEqual(buttons, 3, 4, 5)) return true;
        else if (isEqual(buttons, 6, 7, 8)) return true;
        else if (isEqual(buttons, 0, 3, 6)) return true;
        else if (isEqual(buttons, 1, 4, 7)) return true;
        else if (isEqual(buttons, 2, 5, 8)) return true;
        else if (isEqual(buttons, 0, 4, 8)) return true;
        else if (isEqual(buttons, 6, 4, 2)) return true;
        return false;
    }

    private static boolean isEqual(int[] buttons, int x, int y, int z) {

        if (buttons[x] == buttons[y] && buttons[y] == buttons[z] && buttons[x]!=0) {
            return true;
        } else {
            return false;
        }
    }
}
