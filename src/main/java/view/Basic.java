
package view;

import engine.AfterMoveStatus;
import engine.EngineInterface;

import engine.FieldStatus;
import engine.GameState;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Basic {

    private EngineInterface engineInterface = new GameState();
    private AfterMoveStatus afterMoveStatus;

    @FXML
    private Button btn0;

    @FXML
    private Button btn1;

    @FXML
    private Button btn2;

    @FXML
    private Button btn3;

    @FXML
    private Button btn4;

    @FXML
    private Button btn5;

    @FXML
    private Button btn6;

    @FXML
    private Button btn7;

    @FXML
    private Button btn8;

    @FXML
    private Button btnNGame;

    @FXML
    private Label myLabel;

    @FXML
    void OnClik(ActionEvent event) {
        System.out.println("OK");
//
        Button btn = (Button) event.getSource();
        String id = btn.getId().substring(3, 4);
        System.out.println("sadsadas");


        System.out.println(id);

        afterMoveStatus = engineInterface.onClick(Integer.parseInt(id));

        if (afterMoveStatus.getFieldStatus() == FieldStatus.X) {

            btn.setText("X");

        } else if (afterMoveStatus.getFieldStatus() == FieldStatus.O) {
            btn.setText("O");

        }
        System.out.println(afterMoveStatus.getCommunicate());

        if (!afterMoveStatus.getCommunicate().equals("")) {
            System.out.println("xxxxxxx");
            btn0.setDisable(true);
            btn1.setDisable(true);
            btn2.setDisable(true);
            btn3.setDisable(true);
            btn4.setDisable(true);
            btn5.setDisable(true);
            btn6.setDisable(true);
            btn7.setDisable(true);
            btn8.setDisable(true);
            myLabel.setText(afterMoveStatus.getCommunicate());
        }



    }
    @FXML
    void nGame(ActionEvent event) {

        btn0.setDisable(false);
        btn1.setDisable(false);
        btn2.setDisable(false);
        btn3.setDisable(false);
        btn4.setDisable(false);
        btn5.setDisable(false);
        btn6.setDisable(false);
        btn7.setDisable(false);
        btn8.setDisable(false);

        btn0.setText("");
        btn1.setText("");
        btn2.setText("");
        btn3.setText("");
        btn4.setText("");
        btn5.setText("");
        btn6.setText("");
        btn7.setText("");
        btn8.setText("");
        myLabel.setText("");
        engineInterface = new GameState();

    }
}
